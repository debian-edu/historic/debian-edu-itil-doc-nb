SRCDIR  := $(CURDIR)/documentation
DESTDIR =

all: build

get-orig-source: update

update-copyright:
	$(MAKE) -C $(SRCDIR)/itil update-copyright
	$(MAKE) -C $(SRCDIR)/bokenomlinux update-copyright
	cat debian/copyright.packaging \
		$(SRCDIR)/*/copyright.manual \
		debian/copyright.licence \
		> debian/copyright
	rm documentation/*/copyright.manual

readme status build install dist-clean clean pdf update::
	$(MAKE) -C $(SRCDIR)/itil $@
	$(MAKE) -C $(SRCDIR)/bokenomlinux $@

dist-clean:: clean
