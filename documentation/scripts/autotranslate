#!/usr/bin/perl
#
# Useful background information regarding machine translation and
# copyright can be found at
# * http://en.flossmanuals.net/open-translation-tools/ch057_machine-translation-and-copyright/
# * http://cjlt.dal.ca/vol6_no3/gow.pdf
# * http://blog.fxtrans.com/2009/11/is-google-translate-accurate-enough-for.html
# Alternative approaches can be found at
# * http://pology.nedohodnik.net/
# * http://wiki.apertium.org/wiki/Translating_gettext

use Locale::Po4a::Po;
use LWP::UserAgent;
use HTML::TreeBuilder;
use Data::Dumper;

sub usage {
    my $exit = shift;

    print <<EOF;
Usage: $0 <pofile> [langcode-to] [langcode-from]

Look up untranslated strings in the po file using
http://freetranslation.mobi/ to provide a fuzzy automatic translation
to make it easier to translate.

The default langcode-from is 'en', and the default langcode-to is
'no'.

EOF

    exit($exit) if ($exit);
}

# Trottle access to freetranslation.mobi
my $lastlookup = time();

my $debug = 1;

sub autotranslate {
    my ($froml, $tol, $msg) = @_;
    $msg =~ s/ /%20/g;
    $msg =~ s/\\"/"/g;
    $msg =~ s/&amp;/&/g;

    # Make sure we do not overload the site
    my $minduration = 5;
    my $duration = time() - $lastlookup;
    sleep $minduration - $duration if ($duration < $minduration);
    $lastlookup = time();

    my $url = "http://freetranslation.mobi/$froml-$tol/$msg";
    print "U: $url\n";
    my $ua = LWP::UserAgent->new;
    $ua->timeout(10);
    $ua->env_proxy;
    $ua->agent("API test");

    my $response = $ua->get($url);
    if ($response->is_success) {
        my $html = HTML::TreeBuilder->new;
        $html->parse_content($response->decoded_content);

        # Find the second <span class="word">
        my @info = $html->look_down(_tag => 'span', class=> 'word');
#        print Dumper(@info);
        if (defined $info[1]) {
            my $newmsg = Encode::decode_utf8($info[1]->as_trimmed_text);
            $newmsg =~ s/\"$//;
            $newmsg =~ s/^.\"//;
            $newmsg =~ s/&/&amp;/g;
            $newmsg =~ s/\\"/"/g;
            return $newmsg;
        } else {
            return undef;
        }
    }
    return undef;
}

my $pofile=Locale::Po4a::Po->new();

# Read PO file
my $pofilename = shift || usage(1);
$pofile->read($pofilename);
$pofile->write($pofilename . '.orig');

#my ($froml, $tol) = ('no', 'en'); # nb do not work

# FIXME how can the source and target languages be automatically
# selected?

my $tol   = shift || 'no'; # nb do not work
my $froml = shift || 'en';

# Should tag with mtrans, but it is rejected by Locale::Po4a::Po.
# Using no-wrap instead, to allow search/replace to be done as
# postprocessing.
my $flags = 'fuzzy mtrans no-wrap';

print "Translating to $tol from $froml\n";

my $changes = 0;
for my $id ( 0 .. $pofile->count_entries()
    ) {
    my $msgid = $pofile->msgid($id);

    # Failed to find published API for this.
    my $txt = $pofile->{po}{$msgid}{'msgstr'};

    $msgid =~ s/\\"/"/g;
    print "T $id: '$msgid' -> '$txt'\n";
    if ($msgid && !$txt) {
        print "O: $msgid\n" unless $txt and !$msgid;
        my $newmsg = autotranslate($froml, $tol, $msgid);
        print "N: $newmsg\n";
        if (defined $newmsg) {
            $pofile->push(
                'msgid'  => $msgid,
                'msgstr' => $newmsg,
                'flags'  => $flags,
                );
            $changes = 1;
        }
    }
    if ($changes && 0 == ($id % 5)) {
        $pofile->write($pofilename);
        $changes = 0;
    }
}
print "Done.\n";
$pofile->write($pofilename);
