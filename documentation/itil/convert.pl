#!/usr/bin/env perl

use strict;
use warnings;

my $infile = $ARGV[0] or die "Usage: $0 infile";
my $line;
my $fh;

## Count prefix a specific character in the start of a string
##  to know where in a list we are.
sub count_char {
	my ($ch, $line) = @_;

	my $i = 1;
	while (substr($line, $i, 1) eq "$ch") {
		$i += 1;
	}

	return $i;
}

## Generate a sequence of spaces for use in front of tables.
sub spaces {
	my $count = shift;
	my $spaces = "";

	while ($count > 0) {
		$spaces .= " ";

		$count -= 1;
	}

	return $spaces;
}

## Handle unordered lists.
sub u_list {
	my $line = shift;

	return 0 unless (substr($line, 0, 1) eq '*');

	my $len = count_char('*', $line);
	print spaces($len) . "*" . filter_line(substr($line, $len)) . "\n";

	return 1;
}

## Handle ordered lists.
sub o_list {
	my $line = shift;

	return 0 unless (substr($line, 0, 1) eq '#');

	my $len = count_char('#', $line);
	print spaces($len) . "1." . filter_line(substr($line, $len)) . "\n";

	return 1;
}

# Filtering that should take place both inside and outside tables
sub filter_line {
	my $line = shift;
	# Footnotes have completely different notation
	$line =~ s%<ref>%<<FootNote(%g;
	$line =~ s%</ref>%)>>%g;
	$line =~ s%<references */>%%g;
	$line =~ s%\&quot\;%"%g;
	$line =~ s/\[(https?:[^\]]+) \]/$1/g;
	$line =~ s/\[\[?(https?:[^\] ]+) ([^\]]+)\]\]?/[[$1|$2]]/g;
	return $line;
}

## A very primitive approach to table conversion,
## but it should work for pandoc output.
sub we_can_has_tables {
	#print "||";

	while (($line = <$fh>)) {
		chomp $line;

		if (substr($line, 0, 2) eq '|-') {
			print "||\n";
		} elsif (substr($line, 0, 2) eq '|}') {
			print "||\n";
			return;
		} elsif (substr($line, 0, 1) eq '!') {
			print "||" . substr(filter_line($line), 1);
		} elsif (substr($line, 0, 1) eq '|') {
			print "||" . substr(filter_line($line), 1);
		} else {
			print filter_line($line);
		}
	}
}

open $fh, "<", "$infile"  or die $!;

while (($line = <$fh>)) {
	chomp $line;

	# moinmon uses {{{ }}} instead of <pre></pre>
	$line =~ s%<pre>%\{\{\{\n%g;
	$line =~ s%</pre>%\n\}\}\}%g;

	# moinmon uses ` ` instead of <code></code>
	$line =~ s%<code>%`%g;
	$line =~ s%</code>%`%g;

	# Avoid turning camelcase text into links
	$line =~ s%(\b[A-Z]+[a-z]+[A-Z]+\S*\b)%!$1%g;

	# Turn alerts into moinmoin again
	$line =~ s%\[\[Image:https://wiki.debian.org/htdocs/debwiki/img/alert.png\|/\!\\\]\]%/!\\%g;

	next if (u_list $line); # Unordered list
	next if (o_list $line); # Ordered list

	if (substr($line, 0, 2) eq '{|') {
		we_can_has_tables $line;
		next;
	}
	$line = filter_line($line);

	# <> can be used directly
	$line =~ s%&lt;%<%g;
	$line =~ s%&gt;%>%g;

	print $line . "\n";
}

close $fh;
